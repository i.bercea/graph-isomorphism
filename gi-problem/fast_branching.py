import math

from Auxiliaries.basicpermutationgroup import *
from Auxiliaries.permv2 import permutation
from naive_color_refinement import refine, balanced, coloring
from tree_isomorphism import *


def automorphism(graph):
    """
    Returns the number of automorphism Aut(G), from
    the given graph G.
    :param graph: Graph to cound automorphisms from
    :return: |Aut(G)|
    """
    if max_num_of_edges(len(graph.vertices)) == len(graph.edges):
        return math.factorial(len(graph.vertices))
    G = graph + deepcopy(graph)  # Disjoint graph of G and G'
    for i, v in enumerate(G.vertices[:len(G) // 2]):
        v.id = i
    for i, v in enumerate(G.vertices[len(G) // 2:]):
        v.id = i
    partition = coloring(G)
    X, _, _, _ = generate_aut(G, list(), partition)

    return compute_order(X)


def generate_aut(d_graph, X, partition, D=None, I=None):
    """
    The branching algorithm that applies the pruning algorithm.
    :param d_graph: Disjoint graph of two graphs
    :param X: generator set
    :param D: Distinct sequence containing nodes from V(G)
    :param I: Distinct sequence containing nodes from V(G')
    "return: generator group (rest is rather internally used)
    """
    if I is None:
        I = []
    if D is None:
        D = []
    is_node_trivial = trivial_mapping(D, I)
    partition = sorted(refine(partition), key=lambda l: len(l))
    # partition = refine(partition)
    if balanced(d_graph, partition):
        if len(partition) == len(d_graph.vertices) // 2:  # Bijection/Discrete
            P = permutation(len(d_graph.vertices), partition=partition)
            if not membership(X, P):
                X.append(P)
            return X, D, I, "Bijection"
    else:
        return X, D, I, "Unbalanced"
    # If x_trivial == x, then branching_cell is all nontrivial mappings.
    index, x, x_trivial, branching_cell = next_branching(len(d_graph.vertices), partition)
    if not is_node_trivial:
        if x_trivial is not None:
            partition_copy = mapping(index, x, x_trivial, partition)
            D_copy, I_copy = deepcopy(D) + [x], deepcopy(I) + [x_trivial]
            X, D_copy, I_copy, status = generate_aut(d_graph, X, partition_copy, D_copy, I_copy)
        else:
            partition_copy = mapping(index, x, branching_cell[0], partition)
            D_copy, I_copy = deepcopy(D) + [x], deepcopy(I) + [branching_cell[0]]
            branching_cell.remove(branching_cell[0])
            X, D_copy, I_copy, status = generate_aut(d_graph, X, partition_copy, D_copy, I_copy)
    else:
        if x_trivial is not None:
            partition_copy = mapping(index, x, x_trivial, partition)
            D_copy, I_copy = deepcopy(D) + [x], deepcopy(I) + [x_trivial]
            X, D_copy, I_copy, status = generate_aut(d_graph, X, partition_copy, D_copy, I_copy)

        for y in branching_cell:
            partition_copy = mapping(index, x, y, partition)
            D_copy, I_copy = deepcopy(D) + [x], deepcopy(I) + [y]
            X, D_copy, I_copy, status = generate_aut(d_graph, X, partition_copy, D_copy, I_copy)
            if status == "Bijection" or (D == [] and I == []):
                return X, D_copy, I_copy, "Done"
    return X, D_copy, I_copy, "Done"


def mapping(index, x, y, partition):
    """
    Maps the given vertices as decided by the generate_aut.
    It does so by removing them from their current partition and
    joints them in a seperate one.
    :param index: index of the cell
    :param x: vertex from G
    :param y: vertex from G''
    :return: updated partition
    """
    partition_copy = deepcopy(partition)
    partition_copy.append([x, y])
    partition_copy[index].remove(partition_copy[index][partition[index].index(y)])
    partition_copy[index].remove(partition_copy[index][partition[index].index(x)])

    return partition_copy


# @Deprecated
def create_permutation(n, partition):
    """
    Creates a permutation P that corresponds to
    the bijection obtained by alpha(D,I).
    :param n: The number of vertices in G
    :param partition: Partition after refining alpha(D, I)
    :return: Permutation P corresponding to bijection f: V(G) -> f(G')
    """
    cycles = list()
    for cell in partition:
        if len(cell) == 1:
            cycles.append([cell[0]])
            continue
        cycle = [cell[0].id, cell[1].id] if cell[0].label < n // 2 else [cell[1].id, cell[0].id]
        cycles.append(cycle)
    return permutation(n, cycles=cycles)


def trivial_mapping(D, I):
    for k, u in enumerate(D):
        if u.id != I[k].id:
            return False
    return True


def membership(X, P):
    """
    Membership determines whether a permutation P can be
    obtained by a sequence of composite operations with
    members from the generator group <X>.
    :param X: Set containing generators
    :param P: Permutation found by branching
    :return: True if P is an element of <X>, otherwise False.
    """
    card_generator = len(X)
    if card_generator == 0:
        return P.istrivial()
    elif P.istrivial():
        return True
    elif card_generator == 1 and X[0].istrivial():  # <X> can't generate P if g (in X) is identity
        return False

    alpha = FindNonTrivialOrbit(X)
    orbit_alpha, transversal = Orbit(X, alpha, r_transversal=True)
    if P[alpha] not in set(orbit_alpha):
        return False
    else:
        # Sifting
        stab_alpha = Stabilizer(X, alpha)
        u = transversal[orbit_alpha.index(P[alpha])]
        composition = (-u) * P
        membership(stab_alpha, composition)


def compute_order(X):
    """
    Returns the order (cardinality) of the given
    automorphism group generated by <X>.
    :param X: Generator group of Aut(G)
    :return: Order of Aut(G)
    """
    if len(X) == 1 and X[0].istrivial():
        return 1

    # Note we use Orbit-Stabilizer Theorem: |X| = |Xa|*|aˆX| for any (nontrivial) alpha
    alpha = FindNonTrivialOrbit(X)
    orbit_alpha = Orbit(X, alpha)
    stab_alpha = Stabilizer(X, alpha)
    return len(orbit_alpha) * compute_order(stab_alpha) if len(stab_alpha) != 0 else len(orbit_alpha)


def get_color_class(n, partition):
    """
    Returns a color class C such that the
    cardinality of C is greater or equal to 4,
    with the first encountered branching vertex x.
    :param n: The number of vertices in the graph.
    :param partition: Partition obtained by color refinement.
    :return: (x, <a1,...,aN>) such that x is a branching vertex
    and <a1,....,aN> is the correspoding color class.
    """
    c_class = None
    for i, cell in enumerate(partition):
        if len(cell) >= 4 and len(cell) % 2 == 0:
            c_class = cell
            break
    x = None
    for v in c_class:
        if v.label < n // 2:
            x = v
            break
    return i, x, c_class


def get_branching_cell(n, color_class):
    """
    Returns the branching cell B for any chosen
    branching vertex x of a color C such that for
    all y in B, x.graph != y.graph.
    :param n: The number of vertices in the graph.
    :param color_class: Color class C such that |C|>=4
    :return: Branching cell for x
    """
    cell = []
    for v in color_class:
        if v.label >= n // 2:
            cell.append(v)
    return cell


def next_branching(n, partition):
    diff = n // 2
    x = None
    non_trivial_mapping = None
    epsilon = None
    for i, p in enumerate(partition):
        if len(p) >= 4:
            trivial_mapping = dict()
            for v in p:
                if v.id in trivial_mapping:
                    trivial_mapping[v.id].append(v)
                else:
                    trivial_mapping[v.id] = [v]
            for b_cell in trivial_mapping.values():
                if len(b_cell) == 2:
                    x, epsilon = (b_cell[0], b_cell[1]) if b_cell[0].label < diff else (b_cell[1], b_cell[0])
                    non_trivial_mapping = list()
                    for non_trivial in p:
                        if (non_trivial.label != x.label and non_trivial.label != epsilon.label
                                and non_trivial.label >= diff):
                            non_trivial_mapping.append(non_trivial)
                    return i, x, epsilon, non_trivial_mapping
    # If no trivial mapping could be found, choose any branching vertex and color class C: |C|>=4
    index, x, c_class = get_color_class(n, partition)
    branching_cell = get_branching_cell(n, c_class)
    return index, x, None, branching_cell


def max_num_of_edges(n):
    """
    Returns the maximum number of (possible)
    edges in an loop-free undirected graph G=(V,E).
    :param n: The number of vertices (|V|=n)
    :return: Maximum number of edges, given n.
    """
    return (n ** 2 - 1) / 2


def bucketSort(array):
    """
    Bucket sort is an algorithm that has an
    complexity of B(n)=A(n)=O(n), if the
    input sequence <a', a'',.....,a*> is
    uniformly distributed.
    Used to sort the Partitions <B1, B2,...,Bk>
    lexographically.
    """
    bucket = []
    for i in range(len(array)):
        bucket.append([])

    for j in array:
        index_b = int(10 * j)
        bucket[index_b].append(j)

    for i in range(len(array)):
        bucket[i] = sorted(bucket[i])

    k = 0
    for i in range(len(array)):
        for j in range(len(bucket[i])):
            array[k] = bucket[i][j]
            k += 1
    return array
