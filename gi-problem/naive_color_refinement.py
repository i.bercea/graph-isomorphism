def coloring(graph):
    """
    Naive color refinement

    For each color class (which we will refer to as partition), select the first element in the partition and compare
    its neighbouring partitions with those of all other graphs in the partition. All vertices in the partition which
    have different neighbouring partitions from the first vertex are removed from the partition and placed in a new one.
    The algorithm stops when there is an iteration over the partitions of the graph which did not result in any vertices
    changing partitions.

    :param graph: a Graph object
    :return: Stable partitioning of graph
    """

    partitions = list()
    for vertex in graph.vertices:
        placed = False
        for partition in partitions:
            v = next(iter(partition))
            dg = v.degree
            if vertex not in partition and vertex.degree == dg:
                partition.append(vertex)
                placed = True
                break
        if not placed:
            partitions.append([vertex])

    partitions = refine(partitions)
    return partitions


def balanced(graph, partitions):
    """
    Check if the partitioning of a disjoint union of two graphs is balanced

    :param graph: a Graph object (disjoint union of two graphs, i.e. graph1 + graph2)
    :param partitions: stable partitioning of a graph
    :return: True if the partitioning is balanced, False otherwise
    """

    for partition in partitions:
        count_u, count_v = 0, 0
        for vertex in partition:
            if vertex.label < len(graph) // 2:
                count_u += 1
            else:
                count_v += 1
        if count_u != count_v:
            return False
    return True


def create_new_partition(vertex_neighbours, partition, partitions):
    """
    Create a new partition(i.e. color class)

    All vertices who do not have the neighbouring partitioning as the vertex of index 0 in the current partition are
    placed in a new one. Partitions of length 1 are skipped.

    :param vertex_neighbours: neighbouring partitioning of vertex of index 0 in partition
    :param partition: the partition currently being refined
    :param partitions: the list of all partitions currently existent for a graph
    :return: a tuple is returned; on the first position either True, if a partition has been refined, or False, if no
    change has been made in the current partition; on the second position, the list of the partitions of a graph
    """

    new_partition = list()
    for partition_index in range(1, len(partition)):
        other_vertex_neighbours = get_neighbouring_partitions(partition[partition_index], partitions)
        for neighbour_index in range(len(vertex_neighbours)):
            if vertex_neighbours[neighbour_index] == other_vertex_neighbours[neighbour_index]:
                continue
            if partition[partition_index] in new_partition:
                continue
            new_partition.append(partition[partition_index])
    if new_partition:
        for vertex in new_partition:
            remove_from_partition(vertex, get_partition_index(partition, partitions), partitions)
        partitions.append(new_partition)
        return True, partitions
    return False, partitions


def remove_from_partition(vertex, index, partitions):
    """
    Remove a vertex from a partition

    :param vertex: the Vertex object to be removed from a partition
    :param index: the position in partitions of the partition from which the vertex has to be removed
    :param partitions: the list of partitions of a graph
    """

    partitions[index].remove(vertex)
    if not partitions[index]:
        partitions.remove(partitions[index])


def get_neighbouring_partitions(vertex, partitions):
    """
    Compute neighbouring partitioning of a vertex

    E.g. neighboring_partitions = [0, 2, 0, 1, 1]
    This tells us that vertex has incidence 4 and that there are 5 partitions currently existent for a graph:
        - 0 neighbours in partition 0
        - 2 neighbours in partition 1
        - 0 neighbours in partition 2
        - 1 neighbour in partition 3
        - 1 neighbour in partition 4

    :param vertex: Vertex object for which we compute the neighbouring partitions
    :param partitions: the list of partitions of a graph
    :return: the neighbouring partitioning of a vertex
    """

    neighbouring_partitions = [0] * len(partitions)
    for neighbour in vertex.neighbours:
        for index, partition in enumerate(partitions):
            if neighbour in partition:
                neighbouring_partitions[index] += 1
                break
    return neighbouring_partitions


def get_partition_index(partition, partitions):
    """
    Knowing a partition, get the position at which it can be found in the list of partitions of a graph.

    :param partition: the partition we want to find the index of in partitions
    :param partitions: the list of partitions of a graph
    """

    for index, p in enumerate(partitions):
        if p == partition:
            return index


def refine(partitions):
    """
    Actual partition refinement algorithm called in the methods 'coloring' and 'branching'

    Refines the partitioning of a graph until no partition can be refined anymore. The refining process is based on
    comparing the neighbouring partitions of vertices which currently belong to the same partition.

    The initial partitioning which we get as argument for the first iteration of this method is based on the degrees of
    the vertices belonging to a graph. When called within the branching algorithm, the argument is a list of partitions
    which define a stable, not coarsest, partitioning.

    :param partitions: a list of partitions of a graph
    """

    changed = True
    while changed is True:
        changed = False
        for partition in partitions:
            v = next(iter(partition))
            v_neighbours = get_neighbouring_partitions(v, partitions)
            if len(partition) == 1:
                continue
            elif changed is False:
                changed, partitions = create_new_partition(v_neighbours, partition, partitions)
            else:
                partitions = create_new_partition(v_neighbours, partition, partitions)[1]

    return partitions
