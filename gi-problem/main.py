import timeit
from copy import deepcopy, copy

from Auxiliaries.graph import Graph
from Auxiliaries.graph_io import load_graph
from naive_branching import branching
from naive_color_refinement import coloring
from tree_isomorphism import is_tree, ahu_tree_isomorphism

if __name__ == "__main__":
    start = timeit.default_timer()
    with open('cr_graphs_examples/trees11.grl') as f:
        graphs = []
        for i in range(4):  # Change the range according to the number of graphs in a file (i.e. 4 graphs -> range(4))
            graphs.append(load_graph(f, Graph))

    only_gi = True  # Change to False if you want to count no. automorphisms
    isomorphic = list()
    aut_list = list()
    aut_elem = [0] * len(graphs)
    for i in range(len(graphs)):
        for j in range(i + 1, len(graphs)):
            placed = False
            both = False
            for group in isomorphic:
                if i and j in group:
                    both = True
                    break
                elif i in group or j in group:
                    placed = True
                    break
            if both:
                continue
            if only_gi:
                graph1, graph2 = deepcopy(graphs[i]), deepcopy(graphs[j])
                if is_tree(graph1) and is_tree(graph2):
                    aut = ahu_tree_isomorphism(graph1, graph2)
                else:
                    aut = branching(graphs[i] + graphs[j], coloring(graphs[i] + graphs[j]))
            else:
                aut = branching(graphs[i] + graphs[j], coloring(graphs[i] + graphs[j]))
            if not aut:
                continue
            if placed:
                for index, group in enumerate(isomorphic):
                    if i in group:
                        group.append(j)
                        break
            else:
                isomorphic.append([i, j])
                aut_elem[i], aut_elem[j] = aut, aut
                aut_list.append(aut)

    # Only if there are graphs which do not belong to an isomorphism group (Leave as is if counting #Aut)
    """
    for g in range(len(graphs)):
        placed = False
        for group in isomorphic:
            for g2 in group:
                if g == g2:
                    placed = True
                    break
            if placed:
                break
        if not placed:
            isomorphic.append([g])
            aut = branching(graphs[g] + deepcopy(graphs[g]), coloring(graphs[g] + deepcopy(graphs[g])))
            aut_list.append(aut)
            aut_elem[g] = aut
    """

    # Print statements

    # GI and AUT
    """
    print("Sets of isomorphic graphs: \tNumber of automorphisms:")
    for i, j in zip(isomorphic, aut_list):
        string = '\t\t\t\t\t\t\t'
        if len(i) > 1:
            string = string[:-(len(i) - 1)]
        print(str(i) + string + str(j))
    """

    # GI
    """
    print("Sets of isomorphic graphs:")
    for i in isomorphic:
        print(i)
    """

    # AUT
    """
    print("Graph: \tNumber of automorphisms:")
    for i, j in enumerate(aut_elem):
        if j != 0:
            print(str(i) + ':\t\t' + str(j))
        else:
            print(str(i) + ':\t\t' + str(
                branching(graphs[i] + deepcopy(graphs[i]), coloring(graphs[i] + deepcopy(graphs[i])))))
    """

    stop = timeit.default_timer()
    print('\nRuntime:', stop - start)
