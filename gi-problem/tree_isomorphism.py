import sys
from collections import deque
from copy import deepcopy

sys.setrecursionlimit(999999999)


class TailRecurseException(Exception):
    def __init__(self, args, kwargs):
        self.args = args
        self.kwargs = kwargs


def tail_call_optimized(g):
    """
    This function decorates a function with tail call
    optimization. It does this by throwing an exception
    if it is it's own grandparent, and catching such
    exceptions to fake the tail call optimization.
    This function fails if the decorated
    function recurses in a non-tail context.
    """

    def func(*args, **kwargs):
        f = sys._getframe()
        if f.f_back and f.f_back.f_back \
                and f.f_back.f_back.f_code == f.f_code:
            raise TailRecurseException(args, kwargs)
        else:
            while 1:
                try:
                    return g(*args, **kwargs)
                except TailRecurseException as e:
                    args = e.args
                    kwargs = e.kwargs

    func.__doc__ = g.__doc__
    return func


def ahu_tree_isomorphism(T1, T2):
    """
    Returns a boolean indiciated whether two given trees
    are isomorphic or not.
    :param T1: first tree
    :param T2: second tree
    :return: True iff. f: V(T1) -> V(T2)
    """
    if len(T1.vertices) != len(T2.vertices):
        return False
    if len(T1.edges) != len(T2.edges):
        return False
    r1 = root(T1)
    r2 = root(T2)
    assign_canonical_names(r1)
    assign_canonical_names(r2)
    return r1.tuplename == r2.tuplename


def is_cyclic(center=None, V=None):
    """
    Returns True if G with vertices V contains a
    cycle as a subgraph.
    :return: (Bool, (a1, a2,..., aN)) Tuple with boolean
    indicating whether the graph contains a cycle and a set
    of vertices that are are reachable from V[0]. However,
    only interested in the set if there is no cycle.
    """
    closed = set()

    def cycle(child, visited, parent):
        if parent.label != child.label:
            child.parent = parent
            parent.children.append(child)
        visited |= {parent, child}
        for n in child.neighbours:
            if n not in visited:
                if cycle(n, visited, child):
                    return True, None
            elif n.label != parent.label:
                return True, None
        return False

    root = center
    if root is None:
        for u in V:
            if u.degree == 1:
                root = u
                break
    # root = V[0] if V != None else center # Guess root but if V(G) connected than v -> u for all v,u element of V(G)
    if cycle(root, closed, root):
        return True, None
    return False, closed


def is_tree(G):
    """
    Returns whether the graph G is a tree.
    :return: True iff. no cycle and connected
    """
    cardinality_V = len(G.vertices)
    if cardinality_V == 0 or cardinality_V == 1:
        return True
    elif not cardinality_V - 1 == len(G.edges):
        return False
    else:
        cyclic, reachable = is_cyclic(V=G.vertices)
        return len(reachable) == cardinality_V if not cyclic else False


def root(T):
    """
    Returns the root of T which is used to create a rooted tree
    using the vertices V(T).
    :param T: Tree of which root is to be found
    """
    root = centers_of_tree(T)[0]  # |centers| >= 1 -> pick first
    normalize_tree_structure(T)
    is_cyclic(center=root)  # repurposed here to create tree relationship
    return root


def centers_of_tree(T):
    """
    Finds the center(s) of the Tree T.
    It is used to root the tree and establish its relationship,
    for checking whether it is isomorphic to the other tree.
    :param T: Tree
    :return: centers of the tree T
    """
    Q = deque()
    T_copy = deepcopy(T)
    for v in T_copy.vertices:
        if len(v.children) == 0:
            Q.append(v)
    while len(Q) >= 2:
        u = Q.popleft()
        parent = u.parent
        parent.children.remove(u)
        in_deg = 1 if parent.parent is not None else 0
        if len(parent.children) + in_deg == 1 and not parent in Q:
            Q.append(parent)
    centers = list()
    for center in Q:
        centers.append(T.vertices[center.label])
    return centers


def assign_canonical_names(v):
    """
    Assigns recursively canonical names to the vertices which
    represent the descendant history of the root, i.e. the structure
    of subtrees starting from the childs of root. This is eventually used
    to decide whether two trees are isomorphic or not.
    :param v: vertex to be labeled
    """
    if len(v.children) == 0:
        v.tuplename = "10"
    else:
        for child in v.children:
            assign_canonical_names(child)
    sorted_tuples = sorted(map(lambda k: k.tuplename, v.children))
    temp = "".join(sorted_tuples)
    v.tuplename = "1" + temp + "0"


def normalize_tree_structure(T):
    for u in T.vertices:
        u.parent = None
        u.children = list()


def twin_vertices(V):
    """
    Finds and returns twins and false twins.
    :param V: The vertex set of the disjoint graph.
    :return: Tuple of sets (twins, false_twins)
    """
    false_twins = set()
    twins = set()

    for i in range(0, len(V)):
        for j in range(1, len(V)):
            e_set1, e_set2 = set(V[i].incidence), set(V[j].incidence)
            nbs1, nbs2 = set(V[i].neighbours), set(V[j].neighbours)
            if len(e_set1.intersection(e_set2)) == 1:  # True => exist edge e: e = {V[i], V[j]}
                nbs1 -= {V[j]}
                nbs2 -= {V[i]}
                if len(nbs1.intersection(nbs2)) == len(nbs1):  # True => N(V[i])\{V[j]} == N(V[j])\{V[i]}
                    twins |= {(V[i], V[j])}
            elif V[i] != V[j] and len(nbs1.intersection(nbs2)) == len(nbs1):  # True => N(V[i]) == N(V[j])
                false_twins |= {(V[i], V[j])}
                V[i].faketwin = True
                V[j].faketwin = True

    return twins, false_twins
