from copy import deepcopy
from naive_color_refinement import refine, balanced


def branching(graph, partitions):
    """
    Naive branching algorithm

    In most cases, partition refinement is not enough to decide whether two graphs are isomorphic. Branching deals with
    that and also provides the number of automorphisms of a given graph. This is a brute force approach, which branches
    the partitioning of a graph until it either finds a bijection or it can't branch anymore. The process of naive
    branching can be divide in the following steps:
        1. select a partition of at least 4 elements (since our graph is a disjoint union of two other graphs)
        2. select a vertex belonging to the first graph
        3. iterate over all vertices which belong to the second graph, in the selected partition
        4. assign the selected two vertices to a different partition
        5. partition refine the updated partitions
        6. if the current partitioning is a bijection, increment the #aut
        7. loop over the previous steps

    If a graph in a list of graphs is not isomorphic with any of the others, we can compute the number of automorphisms
    it has by creating a disjoint union of the graph with itself, in order to minimize the conditions we would have to
    add otherwise. A disjoint union of a graph and itself will be the actual graph.

    If we only want to know whether two graphs are isomorphic, we can stop the branching algorithm from computing
    further when it finds the first automorphism. Uncomment the 'break' statement in line 204, if you do not wish to
    count all automorphisms between two graphs.

    :param graph: a Graph object (disjoint union of two graphs)
    :param partitions: the list of partitions of a graph
    :return: the number of automorphisms, if the two graphs are isomorphic, 0 otherwise
    """

    partitions = refine(partitions)

    if balanced(graph, partitions):
        if len(partitions) == len(graph) // 2:
            return 1
    else:
        return 0

    partition_index = None
    for index in range(len(partitions)):
        if len(partitions[index]) >= 4:
            partition_index = index
            break

    vertex_index = None
    for index in range(len(partitions[partition_index])):
        if partitions[partition_index][index].label < len(graph) // 2:
            vertex_index = index
            break

    n = 0
    for index in range(len(partitions[partition_index])):
        if partitions[partition_index][index].label >= len(graph) // 2:
            partitions_copy = deepcopy(partitions)
            partitions_copy.append(
                [partitions_copy[partition_index][vertex_index], partitions_copy[partition_index][index]])
            partitions_copy[partition_index].remove(partitions_copy[partition_index][index])
            partitions_copy[partition_index].remove(partitions_copy[partition_index][vertex_index])

            n += branching(graph, partitions_copy)

            # Comment to count no. automorphisms
            # if n:
            #     break
    return n
