from collections import deque


def dfa_minimization(coloring):
    """
    Returns a partition by using a similar methodology
    as proposed in Hopcrofts dfa minimization. This particular
    algorithm would have been O(nlog(n)) because it does not use
    doubly linked list as a data structure. However, to mention it here
    again, this algorithm has not been sucessfully completed.
    :param coloring: partition of vertices
    :return: partition that refines input coloring
    """
    # P Associates with each color the vertices of this color
    P = dict()
    D = dict()
    for i, p in enumerate(coloring):
        for v in p:
            v.colornum = i
            if i in P:
                P[v.colornum] |= {v}
            else:
                P[v.colornum] = {v}
            D[v] = 0
    c_min, c_max = 0, len(coloring) - 1
    Q = deque(list(range(c_min, c_max + 1)))
    while Q:
        B = dict()
        q = Q.popleft()
        for p in coloring:
            for v in p:
                D[v] = len(P[q].intersection(set(v.neighbours)))
        # Split vertices according to degree w.r.t q
        for i in range(c_min, c_max + 1):
            Bk = dict()
            for v in list(P[i]):
                color = D[v] if D[v] != 0 else v.colornum
                if color in Bk:
                    Bk[color] |= {v}
                else:
                    Bk[color] = {v}
            # All colors except largest
            colors = list(Bk.keys())
            cells = list(Bk.values())

            for c in colors:
                new_color = c_max + c
                if not new_color in Q:
                    Q.append(new_color)
            for l, cell in Bk.items():
                if l in B:
                    B[l] |= cell
                else:
                    B[l] = cell
        c_min, c_max = c_min + 1, c_max + len(B)
        P = dict()
        for b in range(c_min, c_max + 1):
            P[b] = B[b + 1 - c_min]
            for v in list(P[b]):
                v.colornum = b
    partition = list(map(lambda p: list(p), P.values()))
    return partition
