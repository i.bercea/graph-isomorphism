All necessary options that one can alter with can be found commeneted in the code.
Things you would like to adjust:
- print statements (can be found at the end of the main.py); choose whichever fits your needs
- GI problem or #Aut (if you do not wish to compute #Aut, make sure to turn on the break statement within
  naive_branching.py --- there will be an indicative comment)
- change the range according to the number of graphs in a file (at the beginning of the main, adjust the range
  according to the number of graphs there are in the specific file)
- switch 'only_gi' parameter in main to false, together with the other option mentioned above, in order to
  compute #aut